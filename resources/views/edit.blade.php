@extends('master')
@section('content') 
<div class="col-md-12 col-md-offset-5">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
                    
                @endforeach
                </ul>
            </div>
        
            
            @endif
                <form method="post" action="{{route('user.update',$user->id)}}">
            {{ csrf_field() }}
                <input type="text" name="name" placeholder="Enter Name" value="{{$user->name}}"  class="form-control">
                <input type="email" name="email" placeholder="Enter Email" value="{{$user->email}}" class="form-control">
                     <input type="submit" class="btn btn-primary" value="Update">
        
                </form>
        <div>
          