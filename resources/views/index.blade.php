@extends('master')
@section('content') 
<div class="col-md-12 col-md-offset-5">
@if($errors->any())
    <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
            
        @endforeach
        </ul>
    </div>

    
    @endif
        <form method="post" action="{{route('user.store')}}">
    {{ csrf_field() }}
        <input type="text" name="name" placeholder="Enter Name" value="{{old('name')}}" class="form-control">
            <input type="email" name="email" placeholder="Enter Email" value="{{old('email')}}" class="form-control">
            <input type="password" placeholder="Type Password" name="password" class="form-control">
            <input type="submit" class="btn btn-primary" value="Register">

        </form>
<div>
        <div class="table-responsive">
                <table class="table">
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Edit</th>
            <th>Delete</th>

        </tr>
        @foreach($user as $row)
        <tr>

        <td>{{$row->name}}</td>
        <td>{{$row->email}}</td>
        <td><a href="{{route('user.edit',$row->id)}}" class="btn btn-primary">Edit</a></td>

        <td><form method="POST" action="{{route('user.delete',$row->id)}}">
            
        <input type="submit" value="Delete" class="btn btn-danger">
        {{csrf_field()}}
        </form>    
        </td>
    </tr>
        @endforeach
    </table>
</div>

</div>
