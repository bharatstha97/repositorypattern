<?php 

namespace App\Services;

use App\Repositories\Teacher\TeacherInterface;

class TeacherServices{
    protected $teacher;

    //constructor
public function __construct(TeacherInterface $teacher){
    $this->teacher=$teacher;
}

//getting teacher

public function getTeacher(){
    return $this->teacher->get();

}

//finding teacher
public function findTeacher($id){
    return $this->teacher->find($id);
}

//creating teacher
public function createTeacher($values=[]){
    return $this->teacher->create($values);
}
//updating teacher
public function updateTeacher($id, $values=[]){
    return $this->teacher->update($id,$values);
}

//deleting teacher
public function deleteTeacher($id){
    return $this->teacher->delete($id);
}

}
?>