<?php
namespace App\Services;
use App\Repositories\User\UserInterface;
class UserServices{
    protected $user;
    function __construct(UserInterface $user){
        $this->user=$user;
    }
    //creating user
    function createuser($values=[]){
        
        $this->user->create($values);

    }
    function updateuser($values=[],$id){
        return $this->user->userupdate($values,$id);

    }
    function display(){
       return $this->user->display();
    }
    function findUser($id){
        return $this->user->userfind($id);
    }
    function deleteUser($id){
 
        return $this->user->deleteuser($id);
        }
}

?>