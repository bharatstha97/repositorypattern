<?php 
namespace App\Services;
use App\Repositories\Student\StudentInterface;

class StudentServices{
   protected $student;

   public function __construct(StudentInterface $student){
       $this->student=$student;
   }
    //getting students

    public function getStudents(){
        return $this->student->get();
    }

    //finding students

    public function findStudents($id){
        return $this->student->find($id);

    }
    //creating students
    public function createStudents($values=[]){
        return $this->student->create($values);
    }
    //updating students
    public function updateStudents($id, $values=[]){
        return $this->student->update($id,$values);
    }
    //deleting students 
    public function deleteStudents($id){
        return $this->student->delete($id);
    }
}

?>