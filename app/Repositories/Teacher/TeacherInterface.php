<?php 
namespace App\Repositories\Teacher;
interface TeacherInterface{
    public function get();
    public function find($id);
    public function update($id,$values=[]);
    public function delete($id);
    public function create($values=[]);
}
?>