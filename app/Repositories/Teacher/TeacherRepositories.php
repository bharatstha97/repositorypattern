<?php
namespace App\Repositories\Teacher;
use App\Repositories\Teacher\TeacherInterface;
use App\Teacher;
class TeacherRepositories implements TeacherInterface{
    protected $teacher;
    //Constructor
    public function __construct(Teacher $teacher){
        $this->teacher=$teacher;
    }

    //getting teachers
    public function get(){
        return $this->teacher->get();
    }

    //finding teacher
    public function find($id){
        return $this->teacher->find($id);
    }
    //updating teachers
    public function update($id,$values=[]){
        $teacher=$this->teacher->find($id);
        return $teacher->update($values) ? $teacher : false;

    }
    //create teachers
    public function create($values=[]){
        $teacher=new $this->teacher($values);
        return $teacher->save() ? $teacher : false;

    }
    //deleting teachers
    public function delete($id){
        $teacher=$this->teacher->find($id);
        return $teacher->destroy($id) ? true : false;

    }
}
?>