<?php 

namespace App\Repositories\Student;
interface StudentInterface{
    public function get();

    public function find($id);
  
    public function create($values=[]);

    public function delete($id);

    public function update($id,$values=[]);
}
?>