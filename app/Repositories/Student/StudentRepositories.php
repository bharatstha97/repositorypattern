<?php
namespace App\Repositories\Student;
use App\Repositories\Student\StudentInterface;
use App\Student;

class StudentRepositories implements StudentInterface{
protected $student;

//constructor
public function __construct(Student $student){
$this->student=$student;
}
//getting students
public function get(){
return $this->student->get();
}

//find students
public function find($id){
return $this->student->find($id);
}

//creating students
public function create($values=[]){
   
    $student = new $this->student($values);
    return $student->save() ? $student : false;

}

//updating students
public function update($id, $values=[]){
    $student=$this->student->find($id);
    return $student->update($values) ? $student : false;

}

//deleting sutdents
public function delete($id)
{   
    $student=$this->student->find($id);
    return $student->destroy($id) ? true : false;
}

}
?>