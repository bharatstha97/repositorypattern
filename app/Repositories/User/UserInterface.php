<?php
namespace App\Repositories\User;
interface UserInterface{
//user create
public function create($values=[]);
//user update
public function userupdate($values=[],$id);
//user display
public function display();
public function userfind($id);
public function deleteuser($id);
}
?>