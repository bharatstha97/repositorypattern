<?php

namespace App\Providers;
use App\Student;
use App\Repositories\Student\StudentInterface;
use App\Repositories\Student\StudentRepositories;
use App\Teacher;
use App\Repositories\Teacher\TeacherInterface;
use App\Repositories\Teacher\TeacherRepositories;
use App\User;
use App\Repositories\User\UserInterface;
use App\Repositories\User\UserRepository;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);
        $this->app->singleton(UserInterface::class,UserRepository::class);
        $this->app->singleton(StudentInterface::class,StudentRepositories::class);
        $this->app->singleton(TeacherInterface::class,TeacherRepositories::class);
       

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    

    }
}
