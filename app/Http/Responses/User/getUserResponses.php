<?php
namespace App\Http\Responses\User;
use Illuminate\Contracts\Support\Responsable;
class getUserResponses implements Responsable{
  
    protected
        $fail_message = 'Attributes retrieve failed.',
        $success_message = 'Attributes retrieve success.';
    protected
        $message = NULL,
        $data = NULL,
        $code = 500;
    protected
        $user;

        public function __construct($user)
        {
            $this->user= $user;
    
            if($user){
                $this->code = 200;
                $this->message = $this->success_message;
                $this->data = $user;
            }else{
                $this->message = $this->fail_message;
            }
        }

        /**
         * Html response
         */
        public function toResponse($request)
        {
            if($request->expectsJson()) return $this->jsonResponse();
            return $this->htmlResponse();
        }
        public function htmlResponse(){
            if($this->user) return view('index', [
                'user' => $this->user
            ]);
            return redirect()->back()->with('fail_msg', $this->fail_message);
        }


}
?>
