<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/index','UserController@index')->name('index');
Route::get('/teacherindex','TeacherController@index')->name('index');
Route::post('/store','UserController@store')->name('user.store');


Route::get('/edit/{id}','UserController@edit')->name('user.edit');
Route::post('/delete/{id}','UserController@destroy')->name('user.delete');
Route::post('/update/{id}','UserController@update')->name('user.update');